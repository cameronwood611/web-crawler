FROM frostming/pdm

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
            software-properties-common


RUN mkdir -p /bookstoscrape
WORKDIR /bookstoscrape

COPY pdm.lock pyproject.toml /bookstoscrape/
RUN pdm install

COPY ./ /bookstoscrape

ENTRYPOINT [ "pdm", "run", "python", "crawl.py" ]