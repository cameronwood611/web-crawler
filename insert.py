# STL
import os
import logging

# PDM
import asyncpg

# LOCAL
from common import init_logger

LOG = logging.getLogger(__name__)
init_logger("INFO")


async def get_psql_conn() -> asyncpg.Connection:
    config = dict()
    try:
        config = {
            "host": os.environ["PG_HOST"],
            "port": os.environ["PG_PORT"],
            "database": os.environ["PG_DB"],
            "user": os.environ["PG_USER"],
            "password": os.environ["PG_PASS"]
        }
    except KeyError:
        LOG.error(f"Missing environment variables for postgres connection:")

        LOG.error(f"HOST: {os.getenv('PG_HOST')}")
        LOG.error(f"PORT: {os.getenv('PG_PORT')}")
        LOG.error(f"DATABASE: {os.getenv('PG_DB')}")
        LOG.error(f"USER: {os.getenv('PG_USER')}")
        LOG.error(f"PASS: {os.getenv('PG_PASS')}\n")
    finally:
        conn = await asyncpg.connect(**config)
        return conn


async def database_execute(conn: asyncpg.Connection, query: str, args: tuple) -> None:
    await conn.execute(query, *args)