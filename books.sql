CREATE SCHEMA IF NOT EXISTS testing;

CREATE TABLE IF NOT EXISTS testing.book (
    id serial PRIMARY KEY,
    upc text UNIQUE NOT NULL,
    title text NOT NULL,
    genre text NOT NULL,
    price numeric (5,2) NOT NULL
);

GRANT USAGE ON SCHEMA testing TO web_scraper;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA testingn TO web_scraper;
GRANT SELECT, INSERT, UPDATE ON ALL TABLES IN SCHEMA testing TO web_scraper;