#!/bin/bash
# NOTE: These are intentionally run as postgres

echo "Creating database role: web_scraper"
psql -U postgres -c "CREATE USER web_scraper WITH PASSWORD 'changeme'";

echo "Creating database: bookstoscrape"
psql -U postgres -c "CREATE DATABASE bookstoscrape"

psql -d bookstoscrape -U postgres -f books.sql
