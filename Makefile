init:
	pdm install

build:
	docker-compose build

build-force:
	docker-compose build --no-cache --pull

up:
	docker-compose -f docker-compose.yml up

up-prod:
	docker-compose up crawler

up-psql:
	docker-compose up postgres

down:
	docker-compose down