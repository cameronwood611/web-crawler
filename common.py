# STL
import logging
from typing import Any, List

# PDM
import aiohttp
from lxml.html import HtmlElement, fromstring

LOG_FORMAT = (
    "[%(asctime)s] [%(filename)22s:%(lineno)-4s] [%(levelname)8s]   %(message)s"
)


async def get_page(url: str) -> Any:
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            text = await response.text()
            return fromstring(text)

def sxpath(context: HtmlElement, xpath: str) -> List[str]:
    res = context.xpath(xpath, smart_strings=False)
    return res


def init_logger(log_level: str) -> None:
    logging.basicConfig(level=log_level, format=LOG_FORMAT)

