
# STL
import re
import asyncio
import logging
import argparse
from typing import Any, List
from urllib.parse import urljoin

# LOCAL
from common import sxpath, get_page, init_logger
from insert import get_psql_conn, database_execute

LOG = logging.getLogger(__name__)

NEXT_PAGE = './/ul[@class="pager"]/li[@class="next"]/a/@href'
ALL_BOOK_LINKS = './/div[@class="page_inner"]//article/h3/a/@href'
BASE_CONTENT = ''
BOOK_CONTENT = {
    "genre": './/ul[@class="breadcrumb"]/li[position()=3]/a/text()',
    "title": './/article[@class="product_page"]//div[@class="col-sm-6 product_main"]/h1/text()',
    "upc": './/article[@class="product_page"]/table[@class="table table-striped"]/tr[1]/td/text()',
    "price": './/article[@class="product_page"]/table[@class="table table-striped"]/tr[4]/td/text()'
}


def clean_gallery_url(url: str) -> str:
        if "catalogue" not in url:
            return urljoin(url, "/catalogue/page-1.html")
        return url.strip('/').split('/')[-1]

def clean_price(money: str) -> str:
    return re.sub('[^0-9(?:.)]+', '', money)

class Crawler:
    def __init__(self):
        self.BASE_URL = "http://books.toscrape.com"
        self.GALLERY_URL = "http://books.toscrape.com/catalogue/"

    async def start(self, url):
        await self.get_gallery(url)

    async def get_gallery(self, url: str):
        full_url = urljoin(self.GALLERY_URL, url)
        page = await get_page(full_url)
        books = sxpath(page, ALL_BOOK_LINKS)
        for book_url in books:
            await self.get_book(book_url)
        next_page = sxpath(page, NEXT_PAGE)
        if not next_page:
            return None
        await self.get_gallery(next_page[0])

    async def get_book(self, url: str) -> dict:
        item = {}
        url = urljoin(self.GALLERY_URL, url)
        page = await get_page(url)
        for attr, path in BOOK_CONTENT.items():
            item[attr] = sxpath(page, path)[0]
        item['price'] = clean_price(item['price'])
        LOG.info(item)
        conn = await get_psql_conn()
        query = "INSERT INTO testing.book (upc, title, genre, price) VALUES ($1, $2, $3, $4) ON CONFLICT DO NOTHING"
        upc = item['upc']
        title = item['title']
        genre = item['genre']
        price = item['price']
        await database_execute(conn, query, (upc, title, genre, price))
        await conn.close()
        return item


def main(ARGS: argparse.Namespace):
    init_logger("INFO")
    gallery = clean_gallery_url(ARGS.url)
    crawl = Crawler()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(crawl.start(gallery))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A Web Crawler for books.toscrape.com")
    parser.add_argument(
        "-u",
        "--url",
        type=str,
        dest="url",
        default="http://books.toscrape.com/",
        help="Provide a starting gallery url to crawl"
    )
    args = parser.parse_args()
    main(args)